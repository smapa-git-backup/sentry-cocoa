//
//  SentryCrashReportSink.h
//  Sentry
//
//  Created by Daniel Griesser on 10/05/2017.
//  Copyright © 2017 Sentry. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SentryCrash.h"

@interface SentryCrashReportSink : NSObject <SentryCrashReportFilter>

@end
