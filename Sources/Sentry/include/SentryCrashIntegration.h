//
//  SentryCrashIntegration.h
//  Sentry
//
//  Created by Klemens Mantzos on 04.12.19.
//  Copyright © 2019 Sentry. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SentryIntegrationProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface SentryCrashIntegration : NSObject <SentryIntegrationProtocol>

@end

NS_ASSUME_NONNULL_END
