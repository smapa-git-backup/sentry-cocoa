//
//  SentryAutoBreadcrumbTrackingIntegration.h
//  Sentry
//
//  Created by Klemens Mantzos on 05.12.19.
//  Copyright © 2019 Sentry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SentryAutoBreadcrumbTrackingIntegration.h"

NS_ASSUME_NONNULL_BEGIN

/**
* This automatically adds breadcrumbs for different user actions.
*/
@interface SentryAutoBreadcrumbTrackingIntegration : NSObject

@end

NS_ASSUME_NONNULL_END
